package com.cosn.contactfollowup.models;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


public class ContactFollowUp {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;
	
	private String contactId;
	
	private Date date;
	
	private boolean symptoms;

	public ContactFollowUp() {}
	
	public ContactFollowUp(String contactId, Date date, boolean symptoms) {
		this.contactId = contactId;
		this.date = date;
		this.symptoms = symptoms;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isSymptoms() {
		return symptoms;
	}

	public void setSymptoms(boolean symptoms) {
		this.symptoms = symptoms;
	}
	
	@Override
	public String toString() {
		return "ContactFollowUp [id=" + id + ", contactId=" + contactId + ", date=" + date + ", symptoms=" + symptoms
				+ "]";
	}
	
	
	
	
	
}
