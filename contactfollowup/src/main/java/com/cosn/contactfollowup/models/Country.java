package com.cosn.contactfollowup.models;

public class Country {
	String countryISOCode;
	String countryName;
	
	public Country(String iso, String name) {
		this.countryISOCode = iso;
		this.countryName = name;
	}
}
