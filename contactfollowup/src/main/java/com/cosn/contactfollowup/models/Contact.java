package com.cosn.contactfollowup.models;

public class Contact {
	
	String contactID;
	Country nationality;
	String status;
	String familyName, givenName, documentType, documentId, gender, dateOfBirth, phone, adress, latitude, longitude, city, district, state, email, relationshipID, relationshipContactID, relationship;
	int age;
	
	public Contact() {
		
	}
	
	public Contact(String status) {
		this.status = status;
	}
	
	public Contact(String contactID, Country nationality, String status, String familyName, String givenName,
			String documentType, String documentId, String gender, String dateOfBirth, String phone, String adress,
			String latitude, String longitude, String city, String district, String state, String email,
			String relationshipID, String relationshipContactID, String relationship, int age) {
		super();
		this.contactID = contactID;
		this.nationality = nationality;
		this.status = status;
		this.familyName = familyName;
		this.givenName = givenName;
		this.documentType = documentType;
		this.documentId = documentId;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.phone = phone;
		this.adress = adress;
		this.latitude = latitude;
		this.longitude = longitude;
		this.city = city;
		this.district = district;
		this.state = state;
		this.email = email;
		this.relationshipID = relationshipID;
		this.relationshipContactID = relationshipContactID;
		this.relationship = relationship;
		this.age = age;
	}
	public String getContactID() {
		return contactID;
	}
	public void setContactID(String contactID) {
		this.contactID = contactID;
	}
	public Country getNationality() {
		return nationality;
	}
	public void setNationality(Country nationality) {
		this.nationality = nationality;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRelationshipID() {
		return relationshipID;
	}
	public void setRelationshipID(String relationshipID) {
		this.relationshipID = relationshipID;
	}
	public String getRelationshipContactID() {
		return relationshipContactID;
	}
	public void setRelationshipContactID(String relationshipContactID) {
		this.relationshipContactID = relationshipContactID;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Contact [contactID=" + contactID + ", nationality=" + nationality + ", status=" + status
				+ ", familyName=" + familyName + ", givenName=" + givenName + ", documentType=" + documentType
				+ ", documentId=" + documentId + ", gender=" + gender + ", dateOfBirth=" + dateOfBirth + ", phone="
				+ phone + ", adress=" + adress + ", latitude=" + latitude + ", longitude=" + longitude + ", city="
				+ city + ", district=" + district + ", state=" + state + ", email=" + email + ", relationshipID="
				+ relationshipID + ", relationshipContactID=" + relationshipContactID + ", relationship=" + relationship
				+ ", age=" + age + "]";
	}
	

	

}
