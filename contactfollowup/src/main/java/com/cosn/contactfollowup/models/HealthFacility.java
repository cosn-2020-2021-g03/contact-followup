package com.cosn.contactfollowup.models;

public class HealthFacility {
	
	int id;
	String name;
	String fullAddress;
	
	public HealthFacility() {
		this.id = 1;
		this.name = "Hospital São João";
		this.fullAddress = "Alameda Prof. Hernâni Monteiro, 4200-319 Porto";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}
	
	
}
