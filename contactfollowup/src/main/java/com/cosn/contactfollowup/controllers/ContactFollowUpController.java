package com.cosn.contactfollowup.controllers;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.cosn.contactfollowup.models.ContactFollowUp;
import com.cosn.contactfollowup.service.ContactFollowUpService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
public class ContactFollowUpController {

	@Autowired
	ContactFollowUpService contactFollowUpService;


	@Operation(summary = "Find all Contact Follow up meetings in the database", tags = { "all, contact, followup" })
	@GetMapping(value = "/contactFollowUps", produces = MediaType.APPLICATION_JSON_VALUE)
	private ResponseEntity<?> getAllContactFollowUps(){
		
		List<ContactFollowUp> contactFollowUps = contactFollowUpService.getAllContactFollowUps();
		
		
		
		return new ResponseEntity<List<ContactFollowUp>>(contactFollowUps, HttpStatus.OK);
	}
	
	@GetMapping(value = "/contactFollowUps/{contactId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "Find Contact Follow up meetings by contact ID", tags = { "contact, followup, contactid" })
	@ApiResponses(value = {
	            @ApiResponse(responseCode = "200", description = "Follow Up Meeting returned with success"),
	            @ApiResponse(responseCode = "204", description = "No Meetings found for that Contact ID")
	    })
	private ResponseEntity<?> getAllContactFollowUpsFromContactId(@Parameter(description = "id of the contact") @PathVariable("contactId") String contactId){
		
		
		List<ContactFollowUp> followUps = contactFollowUpService.getContactFollowUpsByContactId(contactId);
		
		if(!followUps.isEmpty())
			return new ResponseEntity<List<ContactFollowUp>>(followUps, HttpStatus.OK);
		else {
			return new ResponseEntity<String>("No Follow up related to that Contact ID", HttpStatus.NO_CONTENT);
			
		}
	}

	@Operation(summary = "Create a Contact Follow up meeting with a contact ID and presence of symptoms", tags = { "create, contact, followup, contactid, symptoms" })
	@ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Follow Up Meeting created with success"),
            @ApiResponse(responseCode = "204", description = "No Contact found for that Contact ID"),
            @ApiResponse(responseCode = "409", description = "Error while saving the follow up meeting")
    })
	@PostMapping(value = "/contactFollowUp", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	private ResponseEntity<?> addContactFollowUp(@RequestBody ContactFollowUp contactFollowUp){
		
		boolean contactExists = contactFollowUpService.isContact(contactFollowUp.getContactId());
		
		
		if(!contactExists)
			return new ResponseEntity<String>("No contact with that id", HttpStatus.NOT_FOUND);
		try {
			ContactFollowUp cfu = contactFollowUpService.saveOrUpdateContactFollowUp(contactFollowUp);
			return new ResponseEntity<ContactFollowUp>(cfu, HttpStatus.CREATED);
		}catch(Exception e) {
			return new ResponseEntity<String>("Error while adding creating the follow up", HttpStatus.CONFLICT);
		}
		
	}

}
