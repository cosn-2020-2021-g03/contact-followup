package com.cosn.contactfollowup.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cosn.contactfollowup.client.RestClient;
import com.cosn.contactfollowup.models.Contact;
import com.cosn.contactfollowup.models.ContactFollowUp;
import com.cosn.contactfollowup.repository.ContactFollowUpRepository;

@Service
public class ContactFollowUpService {
	
	RestClient restClient = new RestClient();
	public static final int NR_FOLLOW_UPS = 3;
	
	@Autowired
	ContactFollowUpRepository contactFollowUpRepository;
	
	public ContactFollowUpService(ContactFollowUpRepository contactFollowUpRepository) {
		this.contactFollowUpRepository = contactFollowUpRepository;
	}
	
	public List<ContactFollowUp> getAllContactFollowUps() {
		List<ContactFollowUp> contactFollowUps = new ArrayList<ContactFollowUp>();
		contactFollowUpRepository.findAll().forEach(contactFollowUp -> contactFollowUps.add(contactFollowUp));
		return contactFollowUps;
	}

	public List<ContactFollowUp> getContactFollowUpsByContactId(String contactId) {
		return contactFollowUpRepository.findByContactId(contactId);
	}
	
	public ContactFollowUp saveOrUpdateContactFollowUp(ContactFollowUp contactFollowUp)  {
		contactFollowUp.setDate(new Date());

			ContactFollowUp saved = contactFollowUpRepository.save(contactFollowUp); 

		checkStatus(saved);
		
		return saved; 
	}
	
	public boolean isContact(String contactId) {
		
		return restClient.getContact(contactId).has("contactID");
	}

	public void checkStatus(ContactFollowUp contactFollowUp) {
		int nrFollowUps = getContactFollowUpsByContactId(contactFollowUp.getContactId()).size();
		
		if(contactFollowUp.isSymptoms()) {
			//CREATE CASE
			restClient.createCase(contactFollowUp.getContactId().toString());
			
			//CHANGE STATUS
			Contact c = new Contact("INACTIVE");
			restClient.updateContactStatus(c, contactFollowUp.getContactId().toString());
			
		}else if(nrFollowUps >= NR_FOLLOW_UPS){
			//CHANGE STATUS
			Contact c = new Contact("INACTIVE");
			restClient.updateContactStatus(c, contactFollowUp.getContactId().toString());
		}
		
	}
	
}
