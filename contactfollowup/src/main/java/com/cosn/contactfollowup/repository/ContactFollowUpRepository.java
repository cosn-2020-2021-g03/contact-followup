package com.cosn.contactfollowup.repository;

import java.util.List;
import java.util.Optional;

import com.cosn.contactfollowup.models.ContactFollowUp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface ContactFollowUpRepository extends MongoRepository<ContactFollowUp, Long> {	
	@Override
	List<ContactFollowUp> findAll();

	List<ContactFollowUp> findByContactId(String id);
	
	@Override
	Optional<ContactFollowUp> findById(Long id);

}
