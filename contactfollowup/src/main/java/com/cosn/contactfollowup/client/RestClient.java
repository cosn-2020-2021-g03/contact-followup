package com.cosn.contactfollowup.client;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import com.cosn.contactfollowup.models.Contact;
import com.cosn.contactfollowup.models.HealthFacility;
import com.cosn.contactfollowup.utils.ServicesApi;

public class RestClient {
	
	
	public JSONObject getContact(String contactId) {
		String FINAL_URL = ServicesApi.GET_CONTACT_API.replace("{contactId}", contactId);
		
		
        RestTemplate restTemplate = new RestTemplate();
        
        String result = restTemplate.getForObject(FINAL_URL, String.class);
        
        JSONObject jsonObject = new JSONObject(result);
        
        
        return jsonObject;
		
	}
	
	public void updateContactStatus(Contact updatedContact, String contactId) {
		Map < String, String > params = new HashMap < String, String > ();
        params.put("contactId", contactId);
        
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(ServicesApi.UPDATE_CONTACT_API, updatedContact, params);
	}
	
	public void createCase(String contactId) {
		String FINAL_URL = ServicesApi.CREATE_CASE_API.replace("{contactId}", contactId);
		
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        
        HealthFacility hf = new HealthFacility();
        
        HttpEntity<HealthFacility> entity = new HttpEntity<HealthFacility>(hf, headers);
        
        String result = restTemplate.postForObject(FINAL_URL, entity, String.class);
	}
	
}
