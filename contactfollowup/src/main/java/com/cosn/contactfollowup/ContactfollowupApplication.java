package com.cosn.contactfollowup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class ContactfollowupApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContactfollowupApplication.class, args);
	}

}
