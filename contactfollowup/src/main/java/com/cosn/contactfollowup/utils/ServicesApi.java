package com.cosn.contactfollowup.utils;

public final class ServicesApi {
	  public static final String CREATE_CASE_API = "https://case-service.herokuapp.com/api/case/{contactId}/insert";
	  public static final String UPDATE_CONTACT_API = "https://track-contact.herokuapp.com/contact/{contactId}/update";
	  public static final String GET_CONTACT_API = "https://track-contact.herokuapp.com/contact/{contactId}";
}